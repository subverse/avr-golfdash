//
//Name:      GOLFDASH
//
//Author:    David McKelvie daveatsubversedotcodotnz
//
//Description:  automotive information display/control
//
//Target:    ATMega
//
//Version:    0.1 dev
//
//Functions:
//
//
//
//Hardware Notes:
//
//
//PORT MAP:
//
//    A0  X    B0  X      C0  SCL    D0  RX
//    A1  X    B1  X      C1  SDA    D1  TX
//    A2  X    B2  DS18B20    C2  TCK    D2  X
//    A3  X    B3  OC0      C3  TMS    D3  X
//    A4  X    B4  SS      C4  TDO    D4  OC1B
//    A5  X    B5  MOSI    C5  TDI    D5  OC1A
//    A6  X    B6  MUTE    C6  X    D6  X
//    A7  X    B7  SCK      C7  X    D7  RELAY
//
//PORT NOTES:
//
//    RELAY controls +5V and +12V to peripherals
//
//    DS18B20 is a 1 wire digital thermometer
//
//    LCD is I2C
//
//  Copyright (C) 2005 - 2007 David McKelvie
//
//
///////////////////////////////////////////////////////////////////////////// 

#ifdef F_CPU
#undef F_CPU
#endif
#define F_CPU   16000000UL  // 16 MHz

#include<avr/interrupt.h>
#include<avr/pgmspace.h>
#include<avr/version.h> 
#include<avr/sleep.h>
#include<avr/io.h>
#include<string.h>
#include<stdio.h>
#include<util/delay.h>
#include"iiccharlcd.h"
#include"bargraph.h"
#include"golfdash.h"
#include"onewire.h"
#include"ds18b20.h"
#include"button.h"
#include"adc.h"


void delay_10ms( unsigned int delay )
{
  while( delay -- )
    _delay_ms( 10 );
}


int main( void )
{
  char buffer[ 8 ];
  uint8_t count = 0;
  uint8_t numTempSensors;

  hardware_init();

  numTempSensors = ds18b20_init();

  sei();

  while( 1 )
  {
    bar_write( count++, LCD_LINE_ONE );
    //sprintf( buffer, "%3d %3d", count, adc_read( ADC_CHANNEL_1 ) );
    //sprintf( buffer, "%3d %2x", count, numTempSensors );
    //sprintf( buffer, "%3d %3d", ds18b20_read_temp( 0 ), ds18b20_read_temp( 1 ) );
    //lcd_print( LCD_LINE_TWO, buffer );
    lcd_printf( LCD_LINE_TWO, "%3d %3d", ds18b20_read_temp( 0 ), ds18b20_read_temp( 1 ) );
    delay_10ms( 100 );
  }

  while( 1 );
  {
    go_to_sleep();
  }
}//end main

uint8_t hardware_init( void )
{
  MCUCR  = 0x00;                    //sleep disable, mode idle, INT1 low, INT0 low
  GICR   = 0x00;                    //disable external interrupts
  ADCSRA = 0x00;                    //disable ADC
  ACSR   = _BV(ADC);                  //disable Analog Comparator

  DDRA   = 0x00;                    //make PORTA input
  PORTA  = 0xFF;                    //enable PORTA pullups

  DDRB   = _BV( PB4 ) | _BV( PB5 ) | _BV( PB7 );    //make MOSI, SCK and SS outputs, the rest inputs
  PORTB  = ~_BV(PB2);                    //enable PORTB pullups

  DDRC   = _BV( PC0 ) | _BV( PC1 );          //make SCA and SCL outputs, the rest inputs
  PORTC  = ~( _BV( PC0 ) | _BV( PC1 ) );        //enable SCA and SCL pullups

  DDRD   = _BV( RELAY_PIN );              //make RELAY control output, the rest inputs
  PORTD  = _BV( PD0 ) | _BV( PD1 ) | _BV( PD3 );    //enable RX TX INT1 pullups

  RELAY_OFF();                    // shut off power to peripherals

  SPCR   = _BV( MSTR ) | _BV( SPE ) | _BV(CPOL) | _BV(CPHA);  //enable SPI, master, fosc/4;

  iic_init();
  adc_init();
  lcd_open( LCD_4_BIT_SML, 8, 2 );
  lcd_ioctl( LCD_CLEAR_SCREEN );
  bar_init( 5, 120 );                      // initialise bargraph
  return 1;
}


uint8_t msec_to_ascii( char *timeString, uint32_t time )
{
  timeString[ 0 ] = '0';
  timeString[ 1 ] = '0';
  timeString[ 2 ] = ':';
  timeString[ 3 ] = '0';
  timeString[ 4 ] = '0';
  timeString[ 5 ] =  '\0';
  while( time > HOUR_ONES )    //remove hours
    time -= HOUR_ONES;
  while( time > MINUTE_TENS )    //tens of minutes
  {
    time -= MINUTE_TENS;
    timeString[ 0 ] ++;
  }
  while( time > MINUTE_ONES )    //minutes
  {
    time -= MINUTE_ONES;
    timeString[ 1 ] ++;
  }
  while( time > SECOND_TENS )    //tens of seconds
  {
    time -= SECOND_TENS;
    timeString[ 3 ] ++;
  }
  while( time > SECOND_ONES )    //seconds
  {
    time -= SECOND_ONES;
    timeString[ 4 ] ++;
  }

  return 1;
}

void delay( unsigned int delayTime )
{
  while( delayTime -- );
}

//
//Description:  put the CPU into POWER DOWN state, setup to be
//        woken by low level on INT1.
//
uint8_t go_to_sleep( void )
{
  ADCSRA = 0x00;                    //disable ADC
  ACSR   = _BV( ADC );                //disable Analog Comparator
  MCUCR  = 0x00;                    //sleep disable, mode idle, INT1 low, INT0 low
  cli();                        //disable interrupts
  GICR   = _BV( INT1 );                //enable INT1
  GIFR   = _BV( INTF1 );                //clear INT1 flag
  set_sleep_mode( SLEEP_MODE_PWR_DOWN );
  sleep_enable();
  sei();
  sleep_cpu();
  sleep_disable();
  GICR  = 0;
  return 1;
}

// INT1 ISR, wake up on TACH input
ISR(INT1_vect)
{
  GICR  = 0;    //disable external interrupts
}

//catch unhandled interrupts, halt.
ISR(__vector_default)
{
  while( 1 )
    PORTA ++;
}
