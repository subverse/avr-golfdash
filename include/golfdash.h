//
//
//Name:			GOLFDASH 		
//
//
//
//

#ifndef	__GOLFDASH_H__
#define __GOLFDASH_H__

#include<stdint.h>
#include"iic.h"

#define RELAY_PORT					PORTD
#define RELAY_PIN					PD7
// turn off relay (active high)
#define RELAY_OFF()					(RELAY_PORT&=~_BV(RELAY_PIN))
// turn on relay (active high)
#define RELAY_ON()					(RELAY_PORT|=_BV(RELAY_PIN))

#define	AMP_PORT					PORTD
#define	AMP_PIN						PD7
#define	AMP_OFF()					(AMP_PORT&=~_BV(AMP_PIN))
#define AMP_ON()					(AMP_PORT|=_BV(AMP_PIN))


#define	PLAY_SYMBOL					1
#define PAUSE_SYMBOL				2

#define	HOUR_ONES					3600000
#define	MINUTE_TENS					600000
#define	MINUTE_ONES					60000
#define	SECOND_TENS					10000
#define	SECOND_ONES					1000

uint8_t msec_to_ascii( char *, uint32_t );
uint8_t hardware_init( void );
uint8_t go_to_sleep( void );

#endif /* __GOLFDASH_H__ */
